//listen for auth status change
auth.onAuthStateChanged(user =>
{
    setupUI(user);

    if (user)
    {
        //get data
        db.collection('Lore').onSnapshot(snapshot =>
        {
            setupLore(snapshot.docs);
        });
    }
    else
    {
        setupLore([]);
    }
});

//Create new lore
const createForm = document.querySelector('#create-form');
createForm.addEventListener('submit', (e) =>
{
    e.preventDefault();

    db.collection('Lore').add({
        title: createForm['title'].value,
        content: createForm['content'].value
    }).then(() =>
    {
        //Close and reset form
        const modal = document.querySelector('#modal-create');
        M.Modal.getInstance(modal).close();
        createForm.reset();
    });
});

//sign up user
const signupForm = document.querySelector('#signup-form');

signupForm.addEventListener('submit', (e) =>
{
    e.preventDefault();

    const email = signupForm['signup-email'].value;
    const password = signupForm['signup-password'].value;

    auth.createUserWithEmailAndPassword(email, password).then(cred =>
    {
        const modal = document.querySelector('#modal-signup');
        M.Modal.getInstance(modal).close();
        signupForm.reset();
    });
});

//logout
const logout = document.querySelector('#logout');
logout.addEventListener('click', (e) =>
{
    e.preventDefault();
    auth.signOut().then(() =>
    {
    });
});

const loginForm = document.querySelector('#login-form');
loginForm.addEventListener('submit', (e) =>
{
    e.preventDefault();

    //get user info
    const email = loginForm['login-email'].value;
    const password = loginForm['login-password'].value;

    auth.signInWithEmailAndPassword(email, password).then(cred =>
    {
        //closes window
        const modal = document.querySelector('#modal-login');
        M.Modal.getInstance(modal).close();
        loginForm.reset();
    });
});