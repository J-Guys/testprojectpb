const loreList = document.querySelector('.guides');
const loggedOutLinks = document.querySelectorAll('.logged-out');
const loggedInLinks = document.querySelectorAll('.logged-in');
const accountDetails = document.querySelector('.account-details')

const setupUI = (user) =>
{
  if (user)
  {
    const html = `
      <div>Logged in as ${user.email}</div>
    `;
    accountDetails.innerHTML = html;

    loggedInLinks.forEach(item => item.style.display = 'block');
    loggedOutLinks.forEach(item => item.style.display = 'none');
  }
  else
  {
    accountDetails.innerHTML = '';

    loggedInLinks.forEach(item => item.style.display = 'none');
    loggedOutLinks.forEach(item => item.style.display = 'block');
  }
}

//setup lore
const setupLore = (data) =>
{
  if (data.length !== 0)
  {
    let html = '';

    data.forEach(doc =>
    {
      const lore = doc.data();
      const li = `
        <li>
          <div class="collapsible-header grey lighten-4">${lore.title}</div>
          <div class="collapsible-body white">${lore.content}</div>
        </li>
      `;
      html += li;
    });

    loreList.innerHTML = html;
  }
  else
  {
    loreList.innerHTML = '<h5 class="center-align">Login for EPIC lore</h5>';
  }
}

// setup materialize components
document.addEventListener('DOMContentLoaded', function ()
{
  var modals = document.querySelectorAll('.modal');
  M.Modal.init(modals);

  var items = document.querySelectorAll('.collapsible');
  M.Collapsible.init(items);
});